var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ng_forward_1 = require('ng-forward');
var TestService = (function () {
    function TestService($q, $timeout) {
        this.$q = $q;
        this.$timeout = $timeout;
    }
    TestService.prototype.getValue = function () {
        var _this = this;
        return this.$q(function (resolve) {
            _this.$timeout(function () { return resolve('Async FTW!'); }, 3000);
        });
    };
    TestService = __decorate([
        ng_forward_1.Injectable(),
        ng_forward_1.Inject('$q', '$timeout'), 
        __metadata('design:paramtypes', [Object, Object])
    ], TestService);
    return TestService;
})();
var Nested = (function () {
    function Nested() {
    }
    Nested = __decorate([
        ng_forward_1.Component({ selector: 'nested', template: '<h3>Nested</h3>' }), 
        __metadata('design:paramtypes', [])
    ], Nested);
    return Nested;
})();
var InnerApp = (function () {
    function InnerApp(TestService, $element) {
        this.TestService = TestService;
        this.$element = $element;
        this.event2 = new ng_forward_1.EventEmitter();
        this.event1 = new Event('event1', { bubbles: true });
        this.resolveValue();
    }
    InnerApp.prototype.resolveValue = function () {
        var _this = this;
        this.TestService.getValue().then(function (val) { return _this.num = val; });
    };
    InnerApp.prototype.triggerEventViaEventEmitter = function () {
        this.event2.next();
    };
    InnerApp.prototype.triggerEventViaDOM = function () {
        this.$element.nativeElement.dispatchEvent(this.event1);
    };
    __decorate([
        ng_forward_1.Input(), 
        __metadata('design:type', Object)
    ], InnerApp.prototype, "message1", void 0);
    __decorate([
        ng_forward_1.Input(), 
        __metadata('design:type', Object)
    ], InnerApp.prototype, "message2", void 0);
    __decorate([
        ng_forward_1.Input('message3'), 
        __metadata('design:type', Object)
    ], InnerApp.prototype, "msg3", void 0);
    __decorate([
        ng_forward_1.Output(), 
        __metadata('design:type', Object)
    ], InnerApp.prototype, "event2", void 0);
    __decorate([
        ng_forward_1.Output(), 
        __metadata('design:type', Object)
    ], InnerApp.prototype, "event1", void 0);
    InnerApp = __decorate([
        ng_forward_1.Component({
            selector: 'inner-app',
            directives: [Nested],
            template: "\n        <h2>Inner app</h2>\n        <p>ES7 async resolved value: {{ ctrl.num || 'resolving...' }}</p>\n        <nested></nested>\n\n        <h4>Event</h4>\n        <button (click)=\"ctrl.triggerEventViaEventEmitter()\">\n            Trigger Emitted Event\n        </button>\n        <button (click)=\"ctrl.triggerEventViaDOM()\">\n            Trigger DOM Event\n        </button>\n\n        <h4>One Way String from Parent (read-only)</h4>\n        <p>{{ctrl.msg3}}</p>\n\n        <h4>One Way Binding from Parent (read-only)</h4>\n        <input ng-model=\"ctrl.message1\"/>\n\n        <h4>Two Way Binding to/from Parent (read/write)</h4>\n        <input ng-model=\"ctrl.message2\"/>\n    "
        }),
        ng_forward_1.Inject(TestService, '$element'), 
        __metadata('design:paramtypes', [Object, Object])
    ], InnerApp);
    return InnerApp;
})();
var AppCtrl = (function () {
    function AppCtrl() {
        this.triggers = 0;
        this.message1 = 'Hey, inner app, you can not change this';
        this.message2 = 'Hey, inner app, change me';
    }
    AppCtrl.prototype.onIncrement = function () {
        this.triggers++;
    };
    AppCtrl = __decorate([
        ng_forward_1.Component({
            selector: 'app',
            providers: [TestService, "ui.router"],
            directives: [InnerApp, Nested],
            template: "\n        <h1>App</h1>\n        <nested></nested>\n        <p>Trigger count: {{ ctrl.triggers }}</p>\n\n        <h4>One Way Binding to Child:</h4>\n        <input ng-model=\"ctrl.message1\"/>\n\n        <h4>Two Way Binding to/from Child:</h4>\n        <input ng-model=\"ctrl.message2\"/>\n\n        <hr/>\n        <inner-app (event1)=\"ctrl.onIncrement()\" (event2)=\"ctrl.onIncrement()\"\n                   [message1]=\"ctrl.message1\" [(message2)]=\"ctrl.message2\" message3=\"Hey, inner app... nothin'\">\n        </inner-app>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], AppCtrl);
    return AppCtrl;
})();
ng_forward_1.bootstrap(AppCtrl);
//# sourceMappingURL=index.js.map