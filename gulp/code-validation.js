﻿'use strict';

var path = require('path');
var gulp = require('gulp');
var _    = require('./conf');
var $    = require('gulp-load-plugins')({ lazy : true });
var args = require('yargs').argv;

/*
* JSCS
*/
gulp.task('jscs', function () {
    log('Analyzing source code with JSCS');
    return gulp.src([_.conf.paths.masks.js])
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs());
});

/*
* JSHint
*/
gulp.task('lint', function () {
    log('Analyzing source code with JSLint');
    return gulp.src([_.conf.paths.masks.js])
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jshint('.jshintrc'))
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'))
        .pipe($.notify(function (file) {
            if (file.jshint.success) {
                return {
                    title: 'JSHint',
                    message: 'Passed.'
                };
            }

            var errors = file.jshint.results.map(function (data) {
                if (data.error) {
                    return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
                }
            }).join("\n");
            return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
        }));
});

gulp.task('check-code', ['jscs', 'lint']);

//-------------------------

function log(msg) {
    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    }
    else {
        $.util.log($.util.colors.blue(msg));
    }
}