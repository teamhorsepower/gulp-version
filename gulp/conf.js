var gutil  = require('gulp-util');
var args   = require('yargs').argv;
var gulp   = require('gulp');

var assets = './src/assets/';
var dist   = './dist/';
var dev    = './dev/';

var conf = module.exports.conf = {
    paths: {
        out: {
            js    : env() + 'js',
            css   : env() + 'css',
            img   : env() + 'img',
            fonts : env() + 'fonts'
        },
        test: {
            e2e  : 'e2e',
            unit : 'test'
        }
    },
    files: {
        index : './src/index.html',
        bundle: {
            js  : 'app.js',
            css : 'app.css'
        },
        vendor: {
            js  : 'vendor.js',
            css : 'vendor.css'
        },
        settings: {
            dev  : './src/bundle/dev/settings.js',
            prod : './src/bundle/prod/settings.js'
        },
    },
    env: {
        dist : dist,
        dev  : dev
    },
    masks: {
        css     : assets + 'css/*.css',
        sass    : assets + 'sass/*.scss',
        images  : assets + 'images/**/*.*',
        fonts   : assets + 'fonts/**/*.*',
        js      : './src/**/*.js',
        ts      : './src/**/*.ts',
        extraJs : assets + 'js/*.js',
        html    : './src/*.html',
    }
};

function env(){
    return (args.dist) ? dist : dev;
}

module.exports.env = env;

module.exports.errorHandler = function(title) {
  'use strict';
  return function(err) {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emit('end');
  };
};
