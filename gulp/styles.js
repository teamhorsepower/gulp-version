﻿'use strict';

var path        = require('path');
var gulp        = require('gulp');
var del         = require('del');
var _           = require('./conf');
var bowerFiles  = require('main-bower-files');
var browserSync = require('browser-sync');
var minifyCss   = require('gulp-minify-css');
var args        = require('yargs').argv;
var $           = require('gulp-load-plugins')();

/*------------------------------------------------
 * Removes all the styles of the specific bundle:
 *    /dist if --div is passed as command param.
 *    otherwise /dev
 ----------------------------------------------- */

gulp.task('clean-styles', function () {
    del(path.join(_.env(), '**/*.css'));
});

/*------------------------------------------------
* Compiles, prefixes, minifies and bundles all
* the sass files.
 ----------------------------------------------- */

gulp.task('build-sass', function () {
    return gulp.src(_.conf.masks.sass)
        .pipe($.print())
        .pipe($.if(args.dist, $.sourcemaps.init()))
        .pipe($.sass({ outputStyle: 'expanded' }))
        .on('error', function (err) { displayError(err); })
        .pipe($.autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe($.if(args.dist, minifyCss()))
        .pipe($.concat(_.conf.files.bundle.css))
        .pipe($.if(args.dist, $.sourcemaps.write('maps')))
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest(_.conf.paths.out.css))
        .pipe($.size());
});

/*------------------------------------------------
 * Vendor
 ----------------------------------------------- */

gulp.task('styles', ['clean-styles', 'build-sass'], function () {
    return gulp.src(_.conf.masks.css)
        .pipe($.filter('**/*.css'))
        .pipe($.print())
        .pipe($.concat(_.conf.files.vendor.css))
        .pipe($.flatten())
        .pipe($.if(args.dist, minifyCss()))
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest(_.conf.paths.out.css))
        .pipe($.size());
});

/*------------------------------------------------
 * Sass watcher
 ----------------------------------------------- */

gulp.task('sass-watcher', function() {
    gulp.watch([_.conf.masks.sass], ['styles']);
});

var displayError = function (error) {
    var errorString = '[' + error.plugin + '] ' + error.message.replace("\n", '');

    if (error.fileName){
        errorString += ' in ' + error.fileName;
    }
    if (error.lineNumber){
        errorString += ' on line ' + error.lineNumber;
    }
    console.error(errorString);
}
