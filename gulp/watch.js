'use strict';

var gulp        = require('gulp');
var _           = require('./conf');
var browserSync = require('browser-sync');

function isOnlyChange(event) {
  return event.type === 'changed';
}

gulp.task('watch', ['inject'], function () {
  gulp.watch([_.conf.masks.html, 'bower.json'], ['inject']);

  gulp.watch(_.conf.masks.css, function(event) {
    if(isOnlyChange(event)) {
      browserSync.reload(event.path);
    } else {
      gulp.start('inject');
    }
  });

  gulp.watch( _.conf.masks.js, function(event) {
    if(isOnlyChange(event)) {
      gulp.start('scripts');
    } else {
      gulp.start('inject');
    }
  });

  gulp.watch( _.conf.masks.html, function(event) {
    browserSync.reload(event.path);
  });
});
