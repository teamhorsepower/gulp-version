'use strict';

var gulp = require('gulp');
var _    = require('./conf');
var args = require('yargs').argv;

var $ = require('gulp-load-plugins')();

gulp.task('inject', ['scripts', 'styles'], function () {
  var injectStyles  = gulp.src(_.conf.paths.out.css);
  var injectScripts = gulp.src(_.conf.paths.out.js);

  var injectOptions = {
    ignorePath: args.dist ? 'dist' : 'dev',
    addRootSlash: false
  };

  return gulp.src(conf.paths.indexFile)
    .pipe($.inject(injectStyles, injectOptions))
    .pipe($.inject(injectScripts, injectOptions))
    .pipe(gulp.dest(_.conf.env()));
});
