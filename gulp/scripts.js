'use strict';

var path        = require('path');
var gulp        = require('gulp');
var typescript  = require('gulp-typescript');
var del         = require('del');
var _           = require('./conf');
var browserSync = require('browser-sync');
var addSrc      = require('gulp-add-src');
var merge       = require('merge2');
var args        = require('yargs').argv;
var $           = require('gulp-load-plugins')();

gulp.task('clean-scripts', function () {
    del(path.join(args.dist ? _.conf.env.dist : _.conf.env.dev, '**/*.js'));
    del(path.join(args.dist ? _.conf.env.dist : _.conf.env.dev, '**/*.ts'));
});

gulp.task('scripts', ['clean-scripts'], function () {
    var tsBuildProject = typescript.createProject('tsconfig.json');

    function typescriptToES5(){
        var result = tsBuildProject.src()
            .pipe($.sourcemaps.init())
            .pipe(typescript(tsBuildProject));

        return merge([result.js, result.dts]);
    }

    return typescriptToES5()
        .pipe($.if(args.verbose, $.print()))
        .pipe(addSrc([_.conf.masks.extraJs, _.conf.masks.js, './src/settings.js']))
        .pipe($.flatten())
        .pipe($.if(args.dist, $.uglify().on('error', function(e){ console.log(e); })))
        .pipe($.sourcemaps.write('maps'))
        .pipe(gulp.dest(_.conf.paths.out.js))
        .pipe($.size());

    // .pipe(browserSync.reload({ stream: true }))
});