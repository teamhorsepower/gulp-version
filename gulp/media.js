'use strict';

var gulp        = require('gulp');
var _           = require('./conf');
var imagemin    = require('gulp-imagemin');
var rename      = require("gulp-rename");
var del         = require('del');
var path        = require('path');
var args        = require('yargs').argv;
var $           = require('gulp-load-plugins')();

/*------------------------------------------------
 * Images
 ----------------------------------------------- */

gulp.task('clean-images', function () {
    del(path.join(args.dist ? _.conf.env.dist : _.conf.env.dev, 'img/**/*.*'));
});

gulp.task('images', ['clean-images'], function () {
    return gulp.src(_.conf.masks.images)
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace("assets\\images",'');
        }))
        .pipe(imagemin({ progressive: true }))
        .pipe(gulp.dest(_.conf.paths.out.img));
});

/*------------------------------------------------
 * Fonts
 ----------------------------------------------- */

gulp.task('clean-fonts', function () {
    del(path.join(args.dist ? _.conf.env.dist : _.conf.env.dev, 'fonts/**/*.*'));
});

gulp.task('fonts', ['clean-fonts'], function () {
    return gulp.src(_.conf.masks.fonts)
        .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe($.flatten())
        .pipe(gulp.dest(_.conf.paths.out.fonts));
});