'use strict';

var gulp   = require('gulp');
var wrench = require('wrench');
var jshint = require('gulp-jshint');
var jscs   = require('gulp-jscs');
var notify = require('gulp-notify');
var growl  = require('gulp-notify-growl');

wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});

gulp.task('build', ['styles', 'scripts', 'inject']);

gulp.task('default', ['check-code'], function () {
  gulp.start('build');
});